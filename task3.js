const Readable = require('stream').Readable;
const Writable = require('stream').Writable;
const Transform = require('stream').Transform;

class CReadable extends Readable {
    constructor(options) {
        super(options);
        this.random = 0;
    }

    _read(size) {
        this.random = Math.floor(Math.random() * (100 - 1 + 1)) + 1;
        const result = this.push(this.random.toString());
    }
}

class CWritable extends Writable {
    constructor(options) {
        super(options);
    }

    _write(chunk, encoding, done) {
        console.log(chunk.toString())
        done();
    }
}

class CTransform extends Transform {
    constructor(options) {
        super(options);
    }

    _transform(chunk, encoding, callback) {
        setTimeout(() => {
            this.push(chunk + ' - cipher');
            callback();
        }, 1 * 1000);
    }
}

const rd = new CReadable();
const wr = new CWritable();
const tr = new CTransform();

rd.pipe(tr).pipe(wr);
