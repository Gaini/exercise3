const fs = require('fs');
const crypto = require('crypto');

const ReadStream = fs.createReadStream('read.txt');
const WriteStream = fs.createWriteStream('write.txt');
const Transform = require('stream').Transform;

class CTransform extends Transform {
    constructor(options) {
        super(options);
        this.hash = crypto.createHash('md5');
    }

    _transform(chunk, encoding, callback) {
        this.hash.update(chunk);
        callback();
    }

    _flush(callback) {
        callback(null, this.hash.digest('hex'));
        delete this.hash;
    }

}

const tr = new CTransform();

ReadStream.pipe(tr);

tr.pipe(process.stdout);
tr.pipe(WriteStream);