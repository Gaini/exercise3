const fs = require('fs');
const crypto = require('crypto');

const ReadStream = fs.createReadStream('read.txt');
const WriteStream = fs.createWriteStream('write.txt');

const hash = crypto.createHash('md5').setEncoding('hex');

ReadStream.pipe(hash);

hash.pipe(process.stdout);
hash.pipe(WriteStream);